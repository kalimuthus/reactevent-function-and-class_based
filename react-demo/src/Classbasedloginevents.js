import React from 'react';
import './App.css';
import Buttoncounter from './Buttoncounter';

class Classbasedloginevents extends React.Component{
    constructor(props){
        super()
        console.log(this)

    }
    //using function
    userClick(){
        console.log("class method working")
        console.log(this)

    }
    //using arrow function
    userMyClick = (e) =>{
        console.log("Arrow function working in class based events")
        console.log(e)
    }
    render()
    {
        return(
            // class based need {this} when function call
            //if arrow funcion no need {this.UserMyclick(bind.this)}
            //if  funcion need {this.UserMyclick(bind.this)} beacuse undefined show it
        <div>
            <button onClick={this.userClick} >Click Me </button>
            <Buttoncounter></Buttoncounter>
            
        </div>
        
        )
    }
    
}
export default Classbasedloginevents;